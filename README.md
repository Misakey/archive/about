[![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/48px-GitLab_Logo.svg.png)](https://gitlab.com)

Misakey uses GitLab for the development of its free softwares. Our Github repositories are only mirrors. If you want to work with us, fork us on gitlab.com (no registration needed, you can sign in with your Github account)


# About Misakey

Misakey is the user account solution for people and applications who value privacy and simplicity. And it’s open source.

This repository is the source of the page https://about.misakey.com

## How does it work ?

This documentation uses [docsify](https://docsify.js.org).

You can change the configuration by editing `docs/index.html` file, and content by editing the Markdown files.

### How to run locally

- Clone the repository
- Go to the `docs` directory
- Run `python -m SimpleHTTPServer 3000`
- Enter `localhost:3000` in your browser

:warning: There is no hot reloading, you have to reload your browser when changing a file to see it.

## Contributing

If you see any issue in the doc (incorrect statement, typo, ...), or if you want to enhance it
(doing some design, adding a translation, ...), feel free to fork and propose a Merge Request on 
[Gitlab](https://gitlab.com/Misakey/about).

### Guidelines

- The content of those pages should be understandable for anyone.
- The content should be written in inclusive writing
