# Conditions générales d'utilisation et de vente des services


**Dernière mise à jour:** 9 Mars 2020

<a href="/legals/tos/Misakey-TOS-2020-03-09.pdf" download>Télécharger la version PDF</a>

* [Définitions](#définitions)
* [MENTIONS LÉGALES](#mentions-lÉgales)
* [CGU SITE INTERNET ET SERVICE DE CONSULTATION](#cgu-site-internet-et-service-de-consultation)
* [CGU SERVICES DE GESTION DES COMPTES INTERNET ET SERVICE COLLABORATIF DE CRÉATION ET DE MODIFICATION DES FICHES](#cgu-services-de-gestion-des-comptes-internet-et-service-collaboratif-de-création-et-de-modification-des-fiches)
* [CGV SERVICES PROFESSIONNELS](#cgv-services-professionnels)
* [DISPOSITIONS COMMUNES](#dispositions-communes)


## Définitions

**Documentation Contractuelle :** désigne l'ensemble de la présente documentation contractuelle constituée des définitions, des Mentions Légales, des CGU Site Internet et Service de Consultation, des CGU Services de Gestion des Comptes Internet et Service Collaboratif de Création et de Modification des Fiches, des CGV Services Professionnels et des Dispositions Communes.

**CGU Services de Gestion des Comptes Internet et Service Collaboratif de Création et de Modification des Fiches :** désigne les droits et obligations visés à la section dénommée "CGU Services de Gestion des Comptes Internet et Service Collaboratif de Création et de Modification des Fiches" ci-dessous.

**CGV Services Professionnels :** désigne les droits et obligations visés à la section dénommée "CGV Services Professionnels" ci-dessous.


**CGU Site Internet et Service de Consultation :** désigne les droits et obligations visés à la section dénommée "CGU Site Internet et Service de Consultation" ci-dessous.


**Clé Secrète :** désigne une suite d'octets protégée par le mot de passe de l'Utilisateur, générée à la création du Compte de Données Utilisateur et permettant de chiffrer et déchiffrer les Données Personnelles de l'Utilisateur.

**Compte de Données Utilisateur :** désigne un compte créé par un Utilisateur en vue de l'utilisation des Services de Gestion des Comptes Internet et/ou des Services Professionnels.

**Compte Embarqué :** désigne un compte créé par un Utilisateur en vue de l'authentification et la gestion de ses interactions avec un site Internet ou une application d'une Organisation dans le cadre des services fournis par ladite Organisation audit Utilisateur.

**Contenus Tiers :** désigne les informations contenues dans les Fiches fournies par les Utilisateurs titulaires de Comptes de Données Utilisateurs au titre des Service Collaboratif de Création et de Modification des Fiches (CGU Services de Gestion des Comptes Internet et Service Collaboratif de Création et de Modification des Fiches) et des Services Professionnels (CGV Services Professionnels).

**Demande :** est défini à l'article 4 des CGU Services de Gestion des Comptes Internet et Service Collaboratif de Création et de Modification des Fiches.

**Dispositions Communes :** désigne les droits et obligations visés à la section "Dispositions Communes" ci-dessous.


**Données Personnelles :** désigne toute information se rapportant à l'Utilisateur, personne physique identifiée ou identifiable directement ou indirectement, notamment par référence à un identifiant, tel qu'un nom, un numéro d'identification, des données de localisation, un identifiant en ligne, ou à un ou plusieurs éléments spécifiques propres à son identité physique, physiologique, génétique, psychique, économique, culturelle ou sociale.

**Fiche :** désigne une fiche d'identification d'un site Internet ou d'une application appartenant à une Organisation complétée d'informations sur sa politique de protection de la vie privée de ses utilisateurs.

**LCEN :** désigne la loi n°2004-575 du 21 juin 2004 pour la confiance dans l'économie numérique.


**Législation sur les Données Personnelles :** désigne le règlement (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données et la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés.

**Mentions Légales :** désigne la section dénommée "Mentions Légales" ci-dessous.

**Misakey :** désigne l'entité mentionnée aux Mentions Légales.

**Organisation :** désigne une entité juridique traitant les Données Personnelles d'un Utilisateur, par le biais, par exemple, d'un site Internet.

**Politique de Confidentialité :** désigne la politique de confidentialité de Misakey accessible en cliquant ici: https://www.misakey.com/legals/privacy

**Services :** désigne, ensemble, le Service de Consultation, les Services de Gestion des Comptes Internet, le Service Collaboratif de Création et de Modification des Fiches ainsi que les Services Professionnels.

**Service Collaboratif de Création et de Modification des Fiches :** est défini à l'article 5 des CGU Services de Gestion des Comptes Internet et Service Collaboratif de Création et de Modification des Fiches.

**Service de Consultation :** désigne le service proposé aux Utilisateurs Sans Compte, également accessibles aux Utilisateurs titulaires d'un Compte de Données Utilisateur, mentionné à l'article 6 des CGU Site Internet et Service de Consultation et consistant dans la possibilité de consulter les Fiches.

**Services de Gestion des Comptes Internet :** désigne les services proposés aux Utilisateurs - ayant la qualité de "personnes concernées" au sens de la Législation en Vigueur -  titulaires d'un Compte de Données Utilisateur, détaillés à l'article 4 des CGU Services de Gestion des Comptes Internet et Service Collaboratif de Création et de Modification des Fiches et consistant dans la possibilité de (i) formuler des Demandes auprès des Organisations, relativement à leurs Données Personnelles, (ii) commenter et noter les Organisation sur les Fiches et (iii) utiliser le Service de Consultation sur le Site Internet.

**Services Professionnels :** désigne les services proposés aux Utilisateurs titulaires d'un Compte de Données Utilisateur, professionnels au sens du Code de la consommation, détaillés à l'article 4 des CGV Services Professionnels et consistant dans la possibilité pour leur Organisation de (i) transférer de façon sécurisée des Données Personnelles de ses utilisateurs vers leurs Comptes de Données Utilisateurs afin de satisfaire aux Demandes, (ii) demander des consentements aux Utilisateurs, afin de se conformer à la Législation sur les Données Personnelles, (iii) accéder aux Données Personnelles des Utilisateurs personnes concernées stockées dans leur Compte de Données Utilisateur, avec leur consentement informé (iv) authentifier les Utilisateurs via leur Compte Embarqué (v) gérer sa Fiche en en éditant et en gérant une partie de son contenu et, (vi) administrer et gérer ses rôles et droits d'accès aux Services Professionnels.

**Site Internet :** désigne le site Internet misakey.com ainsi que tous les sous-domaines pouvant s'y rapporter.

**Utilisateur :** désigne toute personne naviguant sur le Site Internet, qu'elle soit titulaire d'un Compte de Données Utilisateur ou non.

**Utilisateur Sans Compte :** désigne toute personne naviguant sur le Site Internet et n'ayant pas créé de Compte de Données Utilisateur.


## MENTIONS LÉGALES


La société Misakey, société par actions simplifiée au capital de 12.500 euros, dont le siège social est situé au 66 avenue des Champs Elysées, 75008 Paris, identifiée au Registre du Commerce et des Sociétés de Paris sous le n°845 272 053, dûment représentée à l'effet des présentes par M. Arthur Blanchon en sa qualité de Président (« **Misakey** »), propose une plateforme de gestion des données personnelles ainsi que des services d'authentification.

Numéro de TVA : FR73845272053

Directeur de la publication : M. Arthur Blanchon

Hébergeur : Amazon France, 67 boulevard du Général Leclerc, 92110 Clichy, France, (01.46.17.10.00)

Pour toute question, vous pouvez contacter Misakey par email à l'adresse suivante : [question.pro@misakey.com](mailto:question.pro@misakey.com).


## CGU SITE INTERNET ET SERVICE DE CONSULTATION

### 1. Objet

Les présentes CGU Site Internet et Service de Consultation régissent (i) les conditions d'utilisation du Site Internet par l'Utilisateur, (ii) les conditions d'utilisation du Service de Consultation par l'Utilisateur et, (iii) les droits et obligations de Misakey et de l'Utilisateur tels que définis ci-après.

### 2. Acceptation

Les présentes CGU Site Internet et Service de Consultation sont communiquées à l'Utilisateur lors de sa connexion au Site Internet.


Toute navigation sur le Site Internet ou exécution du Service de Consultation suppose l'acceptation sans réserve et le respect de l'entièreté des termes des présentes CGU Site Internet et Service de Consultation ainsi que des Dispositions Communes par l'Utilisateur.

Les CGU Site Internet et Service de Consultation  et les Dispositions Communes applicables à l'Utilisateur sont celles en vigueur au jour de consultation du Site Internet. Ces CGU Site Internet et Service de Consultation et Dispositions Communes, telles qu'acceptées par l'Utilisateur restent applicables pendant toute la durée de navigation sur le Site Internet et d'utilisation du Service de Consultation.

### 3. Accès au Site Internet

L'Utilisateur reconnaît disposer de la compétence et des moyens nécessaires pour accéder et utiliser le Site Internet, et notamment d'un accès à l'Internet, préalablement souscrit chez le fournisseur de son choix, dont le coût est à sa charge.

L'Utilisateur reconnaît en particulier :

* que la qualité et la fiabilité des transmissions dépendent des infrastructures de réseaux sur lesquelles les transmissions circulent, et sont aléatoires, ceci pouvant conduire à des pannes ou des saturations des réseaux, plaçant l'Utilisateur dans l'incapacité d'utiliser le Site Internet ;
* qu'il lui appartient d'assurer la sécurité de son équipement terminal et de ses propres données, logiciels ou tout autre équipement à sa disposition, notamment contre toute contamination par virus ou tentative d'intrusion dont il pourrait être victime ;
* que tout équipement connecté au Site Internet est et demeure sous son entière responsabilité, notamment en cas de dommage résultant directement ou indirectement de sa connexion au Site Internet.


Misakey s'efforce de maintenir accessible le Site Internet sans pour autant être tenue à aucune obligation de résultat. L'accès au Site Internet pourra être interrompu à des fins de maintenance, de mise à jour, ou pour toute autre raison d'ordre technique.

### 4. Propriété intellectuelle

L'intégralité des droits de propriété intellectuelle, tels que notamment les droits d'auteur, droits voisins, droits des marques, droits des producteurs de bases de données, portant tant sur la structure que les contenus du Site Internet sont, soit exclusivement détenus par Misakey, soit licenciés à Misakey. Les marques, logos et signes distinctifs appartenant aux Organisations restent leur propriété. Ces derniers ne figurent sur le Site Internet qu'à titre informatif et de référence nécessaire.

Toute reproduction, distribution, destruction, exploitation, téléchargement, diffusion, extraction, transmission, modification ou transformation de tout ou partie du Site Internet sans autorisation préalable et expresse de Misakey, est interdite et constitue une violation de ses droits de propriété intellectuelle.

En conséquence, l'Utilisateur s'engage expressément à :

* n'utiliser le Site Internet qu'à la seule fin de connaître et/ou bénéficier des Services ;
* ne pas porter atteinte aux droits de propriété intellectuelle détenus par Misakey sur son Site Internet, et ses marques ou tout autre signe distinctif, ni à ceux éventuellement détenus par les tiers sur les contenus qu'ils mettent en ligne sur le Site Internet ;
* ne pas copier, par le biais de tout procédé ou logiciel, le contenu du Site Internet ;
* ne pas reproduire, altérer ou modifier le Site Internet, sans le consentement exprès et préalable de Misakey ;
* ne pas extraire ou réutiliser, à des fins commerciales ou privées, sans autorisation écrite et préalable de Misakey, une partie substantielle ou non du contenu du Site Internet et des différents documents qui lui sont envoyés dans le cadre des Services.

Par exception aux stipulations du présent article 4, Misakey concède à l'Utilisateur une licence non-exclusive, gratuite, mondiale et pour la durée des présentes aux fins de télécharger les informations des Fiches publiques sous licence Creative Commons ([https://creativecommons.org/licenses/by/4.0/](https://creativecommons.org/licenses/by/4.0/)) (à l'exclusion des adresses email, commentaires et notes contenus dans les Fiches).

### 5. Liens hypertextes

Le Site Internet peut proposer des liens vers d'autres sites internet. Les sites internet auxquels renvoient ces liens hypertextes sont, sauf mention contraire, indépendants du Site Internet et de Misakey.

Misakey ne saurait en aucune façon être tenue responsable du contenu de ces sites, des dommages qui pourraient résulter de la connexion à ces sites, des informations y figurant ou des transactions pouvant y être réalisées.

### 6. Droit de Consultation

Le Service de Consultation permet à tout Utilisateur, qu'il soit titulaire d'un Compte de Données Utilisateur ou qu'il soit un Utilisateur Sans Compte, de consulter les Fiches.

L'accès au Site Internet et le Service de Consultation sont les seuls Services dont l'Utilisateur Sans Compte peut bénéficier.

Afin de bénéficier des autres Services, l'Utilisateur Sans Compte est invité à créer un Compte de Données Utilisateur.

### 7. Gratuité

L'accès au Site Internet et le Service de Consultation sont fournis à titre gratuit à l'Utilisateur.

## CGU SERVICES DE GESTION DES COMPTES INTERNET ET SERVICE COLLABORATIF DE CRÉATION ET DE MODIFICATION DES FICHES

### 1. Objet

Les présentes CGU Services de Gestion des Comptes Internet et Service Collaboratif de Création et de Modification des Fiches régissent (i) les conditions d'utilisation des Services de Gestion des Comptes Internet et du Service Collaboratif de Création et de Modification des Fiches par l'Utilisateur titulaire d'un Compte de Données Utilisateur et, (ii) les droits et obligations de Misakey et de l'Utilisateur titulaire d'un Compte de Données Utilisateur tels que définis ci-après.

### 2. Acceptation

Les CGU Services de Gestion des Comptes Internet et Service Collaboratif de Création et de Modification des Fiches sont communiquées à l'Utilisateur lors de sa connexion au Site Internet et sont en outre disponibles lors de la création du Compte de Données Utilisateur.

En cliquant sur "Accepter", l'Utilisateur titulaire d'un Compte de Données Utilisateur reconnaît avoir pris connaissance et déclare accepter toutes les stipulations des CGU Services de Gestion des Comptes Internet et Service Collaboratif de Création et de Modification des Fiches ainsi que des Dispositions Communes.

Toute exécution des Services de Gestion des Comptes Internet et du Service Collaboratif de Création et de Modification des Fiches suppose l'acceptation sans réserve et le respect de l'entièreté des termes des présentes CGU Services de Gestion des Comptes Internet et Service Collaboratif de Création et de Modification des Fiches et des Dispositions Communes par l'Utilisateur titulaire d'un Compte de Données Utilisateur.

Les CGU Services de Gestion des Comptes Internet et Service Collaboratif de Création et de Modification des Fiches et les Dispositions Communes applicables sont celles en vigueur au jour de leur acceptation par l'Utilisateur titulaire d'un Compte de Données Utilisateur. Les CGU Services de Gestion des Comptes Internet et Service Collaboratif de Création et de Modification des Fiches et les Dispositions Communes, telles qu'acceptées par l'Utilisateur titulaire d'un Compte de Données Utilisateur, restent applicables pendant toute la durée d'utilisation des Services de Gestion des Comptes Internet et du Service Collaboratif de Création et de Modification des Fiches.

### 3. Création du Compte de Données Utilisateur

L'utilisation des Services de Gestion des Comptes Internet et du Service Collaboratif de Création et de Modification des Fiches par l'Utilisateur nécessite que celui-ci créé un Compte de Données Utilisateur.

Pour ce faire, l'Utilisateur doit valider un identifiant : e-mail et/ou un numéro de téléphone.

Un courriel et/ou un SMS de confirmation de l'identifiant sera automatiquement envoyé à l'adresse e-mail et/ou au numéro de téléphone, ledit identifiant n'étant validé que par la retranscription sur l'interface de création du Compte de Données Utilisateur du code secret contenu dans ce courriel ou le SMS par l'Utilisateur.

L'Utilisateur pourra compléter son profil afin d'améliorer la sécurité et la qualité de son expérience de l'usage des services Misakey.

Une Clé Secrète est générée à la création du Compte de Données Utilisateur et protégée par le mot de passe de l'Utilisateur.

L'Utilisateur demeure l'unique responsable de l'usage qui pourrait être fait de son Compte de Données Utilisateur, et s'engage par conséquent à conserver son identifiant, son mot de passe et les codes de confirmation envoyés par e-mail et/ou SMS strictement confidentiels.

L'adresse email et/ou le numéro de téléphone indiqués doivent rester valides aussi longtemps que le Compte de Données Utilisateur existe afin que l'Utilisateur puisse bénéficier des Services de Gestion des Comptes Internet et du Service Collaboratif de Création et de Modification des Fiches.

### 4. Utilisation des Services de Gestion des Comptes Internet

Misakey propose à l'Utilisateur titulaire d'un Compte de Données Utilisateur des Services de Gestion des Comptes Internet permettant de:
* formuler auprès des Organisations, relativement à leurs Données Personnelles et conformément à la Législation sur les Données Personnelles, des demandes de (les "Demandes") droit à la portabilité;
* commenter et noter les Organisations sur les Fiches;
* utiliser le Service de Consultation sur le Site Internet.

Les Demandes pourront uniquement être formulées par un email pré-rédigé par Misakey à destination des Organisations référencées sur le Site Internet et dont la Fiche comporte une adresse email destinataire validée par Misakey.
Les Demandes utilisent l'email de l'utilisateur comme clé d'identification pour le responsable de traitement des données de l'Organisation.

Si la Demande consiste en l'invocation du droit à la portabilité tel que prévu par la Législation sur les Données Personnelles, l'email de demande à l'Organisation comportera un lien vers un espace sécurisé permettant à celle-ci de télécharger les Données Personnelles de l'Utilisateur et de les chiffrer localement avec la Clé Secrète de l'Utilisateur afin que Misakey ou toutes personnes ayant accès aux fichiers transférés ne puisse déchiffrer son contenu. Le fichier contenant les données personnelles de l'Utilisateur ne sera consultable et téléchargeable par l'Utilisateur que par l'intermédiaire du Compte de Données Utilisateur qui permet le déchiffrement dudit fichier avec la Clé Secrète de l'Utilisateur.

### 5. Service Collaboratif de Création et de Modification des Fiches

Sous les mêmes conditions que pour les Services de Gestion des Comptes Internet, l'Utilisateur titulaire d'un Compte de Données Utilisateur pourra également bénéficier des services suivants (le "**Service Collaboratif de Création et de Modification des Fiches**"):
* créer de nouvelles Fiches pour des Organisation ne disposant pas déjà d'une Fiche;
* compléter ou mettre à jour les Fiches existantes;

A cet effet, l'Utilisateur du Service Collaboratif de Création et de Modification des Fiches concède à Misakey une licence non-exclusive, gratuite, mondiale et pour la durée des droits d'auteur en France aux fins de représenter, reproduire et mettre à disposition du public via ses Services les parties des fiches créées par lui ainsi que toutes modifications, commentaires et notes qu'il y apporte.

Dans ce cadre, l'Utilisateur du Service Collaboratif de Création et de Modification des Fiches déclare qu'il jouit de la pleine capacité de consentir la présente licence ainsi que de tous les droits de propriété intellectuelle y afférents et que rien ne s'oppose de son chef à la conclusion de celle-ci. Il garantit Misakey contre tous troubles de jouissance provenant de son fait personnel ou du fait de tout tiers.

### 6. Contenus Tiers

L'Utilisateur reconnaît que le Service de Gestion des Comptes Internet et le Service Collaboratif de Création et de Modification des Fiches consistant notamment en la possibilité de commenter et noter les Organisations sur les Fiches et créer et modifier des Fiches impliquent que les Contenus Tiers proviennent d'Utilisateurs et non de Misakey.

Dès lors, Misakey ne peut être considérée comme ayant la qualité d'éditeur des Contenus Tiers mais exclusivement celle d'hébergeur qui consiste à mettre à la disposition des Utilisateurs des moyens techniques permettant le stockage direct et permanent d'informations destinées à être communiquées au public.

Misakey répond en cela à la définition de l'article 6.I.2 de la LCEN.

Le paragraphe 5 du I de l'article 6 de la LCEN précise que :

« La connaissance des faits litigieux est présumée acquise par les personnes désignées au 2 lorsqu'il leur est notifié les éléments suivants : - la date de la notification ; - si le notifiant est une personne physique : ses nom, prénoms, profession, domicile, nationalité, date et lieu de naissance ; si le requérant est une personne morale : sa forme, sa dénomination, son siège social et l'organe qui la représente légalement ; les nom et domicile du destinataire ou, s'il s'agit d'une personne morale, sa dénomination et son siège social ; - la description des faits litigieux et leur localisation précise ; - les motifs pour lesquels le contenu doit être retiré, comprenant la mention des dispositions légales et des justifications de faits ; - la copie de la correspondance adressée à l'auteur ou à l'éditeur des informations ou activités litigieuses demandant leur interruption, leur retrait ou leur modification, ou la justification de ce que l'auteur ou l'éditeur n'a pu être contacté. ».

Misakey réagit donc uniquement a posteriori dès lors que lui est signalé le caractère prétendument illicite ou indélicat d'un Contenu Tiers dans les conditions prévues au paragraphe 5 du I de l'article 6 de la LCEN indiquées ci-dessus et met en œuvre promptement les mesures nécessaires pour que le Contenu Tiers ne soit plus accessible. Ces mesures peuvent aller de la suppression du Contenu Tiers au bannissement temporaire voire définitif d'un Utilisateur titulaire d'un Compte de Données Utilisateur.

De même, Misakey ne procède pas à une surveillance générale des Contenus Tiers au-delà du concours à la répression de l'apologie des crimes contre l'humanité, de l'incitation à la haine raciale ainsi que de la pornographie enfantine, de l'incitation à la violence, notamment l'incitation aux violences faites aux femmes, ainsi que des atteintes à la dignité humaine conformément aux dispositions du paragraphe 7 du I de l'article 6 de la LCEN.

L'Utilisateur titulaire d'un Compte de Données Utilisateur s'engage à respecter l'ensemble de la législation en vigueur et à ne pas porter atteinte aux droits de tiers, et notamment :

* à ce que les Contenus Tiers ne portent atteinte en aucune façon aux droits que les tiers, personnes physiques ou morales, pourraient détenir notamment en matière de propriété industrielle, de droits d'auteur ou de droits voisins, du droit sui generis applicable aux bases de données, du droit à l'image ou encore du droit au respect de la vie privée;
* à ne pas publier des Contenus Tiers injurieux, diffamatoires ou racistes, attentatoires aux bonnes mœurs, de Contenus Tiers à caractère violent ou pornographique, de Contenus Tiers constitutifs d'apologie des crimes contre l'humanité, de négation de génocides, d'incitation à la violence, à la haine raciale ou à la pornographie infantile, de Contenus Tiers susceptibles de porter atteinte d'une quelconque manière aux utilisateurs mineurs, de les inciter à se mettre en danger d'une quelconque manière, de Contenus Tiers susceptibles par leur nature de porter atteinte au respect de la personne humaine, de sa dignité, de l'égalité entre les femmes et les hommes, de la protection des enfants et des adolescents. Il s'engage également à ne pas publier de Contenus Tiers encourageant la commission de crimes et/ou délits ou incitant à la consommation de substances interdites, de Contenus Tiers incitant à la discrimination, à la haine ou la violence.
7. Gratuité
Les Services de Gestion des Comptes Internet et le Service Collaboratif de Création et de Modification des Fiches sont fournis à titre gratuit à l'Utilisateur titulaire d'un Compte de Données Utilisateur.
8. Résiliation
L'Utilisateur titulaire d'un Compte de Données Utilisateur peut à tout moment résilier sa relation contractuelle avec Misakey en utilisant la fonction « Supprimer le Compte » présente sur le Site Internet.


## CGV SERVICES PROFESSIONNELS

### 1. Objet

Les présentes CGV Services Professionnels régissent (i) les conditions d'utilisation des Services Professionnels par l'Utilisateur titulaire d'un Compte de Données Utilisateur et, (ii) les droits et obligations de Misakey et de l'Utilisateur des Services Professionnels tels que définis ci-après.

Misakey attire l'attention de l'Utilisateur titulaire d'un Compte de Données Utilisateur sur le fait que les Services Professionnels ainsi que les présentes CGV Services Professionnels s'adressent exclusivement à des Utilisateurs:
* titulaires d'un Compte de Données Utilisateur;
* ayant la qualité de professionnels au sens du Code de la consommation;
* agissant pour le compte d'une Organisation.

### 2. Acceptation

Les CGV Services Professionnels sont communiquées à l'Utilisateur lors de sa connexion au Site Internet et sont en outre disponibles lors de la création du Compte de Données Utilisateur.

En cliquant sur "Accepter", l'Utilisateur titulaire d'un Compte de Données Utilisateur reconnaît avoir pris connaissance et déclare accepter toutes les stipulations des CGV Services Professionnels ainsi que les Dispositions Communes.

Toute exécution des Services Professionnels suppose l'acceptation sans réserve et le respect de l'entièreté des termes des présentes CGV Services Professionnels et des Dispositions Communes par l'Utilisateur des Services Professionnels.

Les CGV Services Professionnels et les Dispositions Communes applicables sont celles en vigueur au jour de leur acceptation par l'Utilisateur des Services Professionnels. Les CGV Services Professionnels et les Dispositions Communes, telles qu'acceptées par l'Utilisateur des Services Professionnels, restent applicables pendant toute la durée d'utilisation des Services Professionnels.

L'Utilisateur souscrivant aux Services Professionnels certifie qu'il accepte les CGV Services Professionnels et les Dispositions Communes au nom et pour le compte d'une Organisation, et certifie qu'il en a le droit et l'autorité.

### 3. Création du Compte de Données Utilisateur

L'utilisation des Services Professionnels par l'Utilisateur nécessite que celui-ci créé un Compte de Données Utilisateur.

Pour ce faire, l'Utilisateur doit valider un identifiant : e-mail et/ou un numéro de téléphone.

Un courriel et/ou un SMS de confirmation de l'identifiant sera automatiquement envoyé à l'adresse e-mail et/ou au numéro de téléphone, ledit identifiant n'étant validé que par la retranscription sur l'interface de création du Compte de Données Utilisateur du code secret contenu dans ce courriel ou le SMS par l'Utilisateur.

L'Utilisateur devra compléter son profil afin de confirmer le(s) site(s) et le(s) application(s) dont il est l'administrateur ainsi que la/les organisations qu'il représente.

Une Clé Secrète est générée à la création du Compte de Données l'Utilisateur et protégée par le mot de passe de l'Utilisateur.

Les informations renseignées doivent être complètes, exactes et à jour, ce dont l'Utilisateur se porte garant. En cas de modification de l'une des informations renseignées, l'Utilisateur s'engage à mettre à jour ses informations afin de maintenir leur exactitude.

L'Utilisateur demeure l'unique responsable de l'usage qui pourrait être fait de son Compte de Données Utilisateur, et s'engage par conséquent à conserver son identifiant et son mot de passe strictement confidentiels.

L'adresse email et/ou le numéro de téléphone indiqués doivent rester valides aussi longtemps que le Compte de Données Utilisateur existe afin que l'Utilisateur puisse bénéficier des Services Professionnels.

### 4. Utilisation des Services Professionnels

Misakey propose à l'Utilisateur titulaire d'un Compte de Données Utilisateur des Services Professionnels permettant à son Organisation de:
* transférer de façon sécurisée des Données Personnelles de ses utilisateurs vers leurs Comptes de Données Utilisateurs afin de satisfaire aux Demandes;
* demander des consentements aux Utilisateurs, afin de se conformer à la Législation sur les Données Personnelles (le "**Service de Demande de Consentement**");
* accéder aux Données Personnelles des Utilisateurs personnes concernées stockées dans leur Compte de Données Utilisateur, avec leur consentement informé (le "**Service d'Accès aux Données**");
* authentifier des Utilisateurs via leur Compte Embarqué;
* gérer sa Fiche en en éditant et en gérant une partie de son contenu;
* administrer et gérer ses rôles et droits d'accès aux Services Professionnels.

### 5. Prix

Tous les Services Professionnels sont gratuits, à l'exception du Service de Demande de Consentement informé, selon la tarification suivante:
[Ce service n'est pas encore actif]

Le paiement du prix doit être réalisé en ligne à partir du Site Internet par carte bancaire, préalablement à la réalisation du Service de Demande de Consentement. Toute demande de paiement par un autre mode de paiement pourra être refusée par Misakey.

Une fois les coordonnées bancaires de paiement renseignées, l'Utilisateur des Services Professionnels est invité à vérifier et valider sa commande.

Misakey se réserve le droit de modifier ses prix à tout moment.

Tous les prix sont stipulés hors taxes, taxes en sus à la charge de l'Utilisateur des Services Professionnels.

### 6. Contenus Tiers

L'Utilisateur des Services Professionnels reconnaît que les Services Professionnels consistant notamment en la possibilité de revendiquer la responsabilité de l'Organisation sur le Site Internet et/ou l'application en vue d'administrer une partie du contenu de la Fiche associée implique que les Contenus Tiers proviennent d'Utilisateurs et non de Misakey.

Dès lors, Misakey ne peut être considérée comme ayant la qualité d'éditeur des Contenus Tiers mais exclusivement celle d'hébergeur qui consiste à mettre à la disposition des Utilisateurs des Services Professionnels des moyens techniques permettant le stockage direct et permanent d'informations destinées à être communiquées au public.

Misakey répond en cela à la définition de l'article 6.I.2 de la LCEN.

Le paragraphe 5 du I de l'article 6 de la LCEN précise que :

« La connaissance des faits litigieux est présumée acquise par les personnes désignées au 2 lorsqu'il leur est notifié les éléments suivants : - la date de la notification ; - si le notifiant est une personne physique : ses nom, prénoms, profession, domicile, nationalité, date et lieu de naissance ; si le requérant est une personne morale : sa forme, sa dénomination, son siège social et l'organe qui la représente légalement ; les nom et domicile du destinataire ou, s'il s'agit d'une personne morale, sa dénomination et son siège social ; - la description des faits litigieux et leur localisation précise ; - les motifs pour lesquels le contenu doit être retiré, comprenant la mention des dispositions légales et des justifications de faits ; - la copie de la correspondance adressée à l'auteur ou à l'éditeur des informations ou activités litigieuses demandant leur interruption, leur retrait ou leur modification, ou la justification de ce que l'auteur ou l'éditeur n'a pu être contacté. ».

Misakey réagit donc uniquement a posteriori dès lors que lui est signalé le caractère prétendument illicite ou indélicat d'un Contenu Tiers dans les conditions prévues au paragraphe 5 du I de l'article 6 de la LCEN indiquées ci-dessus et met en œuvre promptement les mesures nécessaires pour que le Contenu Tiers ne soit plus accessible. Ces mesures peuvent aller de la suppression du Contenu Tiers au bannissement temporaire voire définitif d'un Utilisateur titulaire d'un Compte de Données Utilisateur.

De même, Misakey ne procède pas à une surveillance générale des Contenus Tiers au-delà du concours à la répression de l'apologie des crimes contre l'humanité, de l'incitation à la haine raciale ainsi que de la pornographie enfantine, de l'incitation à la violence, notamment l'incitation aux violences faites aux femmes, ainsi que des atteintes à la dignité humaine conformément aux dispositions du paragraphe 7 du I de l'article 6 de la LCEN.

L'Utilisateur des Services Professionnels s'engage à respecter l'ensemble de la législation en vigueur et à ne pas porter atteinte aux droits de tiers, et notamment :

* à ce que les Contenus Tiers ne portent atteinte en aucune façon aux droits que les tiers, personnes physiques ou morales, pourraient détenir notamment en matière de propriété industrielle, de droits d'auteur ou de droits voisins, du droit sui generis applicable aux bases de données, du droit à l'image ou encore du droit au respect de la vie privée;
* à ne pas publier des Contenus Tiers injurieux, diffamatoires ou racistes, attentatoires aux bonnes mœurs, de Contenus Tiers à caractère violent ou pornographique, de Contenus Tiers constitutifs d'apologie des crimes contre l'humanité, de négation de génocides, d'incitation à la violence, à la haine raciale ou à la pornographie infantile, de Contenus Tiers susceptibles de porter atteinte d'une quelconque manière aux utilisateurs mineurs, de les inciter à se mettre en danger d'une quelconque manière, de Contenu Tiers susceptibles par leur nature de porter atteinte au respect de la personne humaine, de sa dignité, de l'égalité entre les femmes et les hommes, de la protection des enfants et des adolescents. Il s'engage également à ne pas publier de Contenus Tiers encourageant la commission de crimes et/ou délits ou incitant à la consommation de substances interdites, de Contenus Tiers incitant à la discrimination, à la haine ou la violence.

### 7. Obligations de l'Utilisateur des Services Professionnels

L'Utilisateur des Services Professionnels:
* sollicite de la part de l'Utilisateur ayant fait une Demande à l'Organisation dont il a la charge toute information utile afin de satisfaire à ladite Demande ;
* satisfait aux Demandes selon les règles de l'art, les normes existantes et les lois et règlements en vigueur, et notamment la Législation sur les Données Personnelles, en professionnel ayant une longue expérience de telles réalisations;
* s'engage à respecter les obligations de transparence vis-à-vis des Utilisateurs personnes concernées prévues par la Législation sur les Données Personnelles, notamment en ce qui concerne les zones de stockage desdites données ainsi que leurs durées de conservation;
* s'engage à minimiser le stockage des Données Personnelles;
* fera ses meilleurs efforts pour automatiser le traitement des Demandes afin d'y répondre dans un délai maximum de 24 heures et en respectant les formats spécifiés par Misakey, à défaut, l'Utilisateur des Services Professionnels ne saurait bénéficier du Service d'Accès aux Données;
* devra, préalablement à toute demande d'accès aux Données Personnelles d'un Utilisateur contenues dans son Compte de Données Utilisateur dans le cadre du Service d'Accès aux Données, solliciter de la part dudit Utilisateur son consentement via le site Internet ou l'application de son Organisation, conformément et au travers du Service de Demande de Consentement;
* devra, préalablement à toute demande d'accès aux Données Personnelles d'un Utilisateur contenues dans son Compte de Données Utilisateur dans le cadre du Service d'Accès aux Données, informer ledit Utilisateur sur les zones de stockage desdites données ainsi que leurs durées de conservation via le site Internet ou l'application de son Organisation, conformément et au travers du Service de Demande de Consentement;
* s'engage à n'utiliser les données de l'utilisateur que pour l'usage et dans un cadre légal de traitement pour lequel l'utilisateur a donné son consentement au travers du Service de Demande de Consentement de Misakey.

### 8. Responsabilité

De manière générale, les parties conviennent expressément que Misakey ne pourra être tenue responsable des interruptions de services ou des dommages liés :
* au mauvais fonctionnement des équipements ou logiciels appartenant ou utilisés par l'Utilisateur des Services Professionnels ;
* à une utilisation anormale, ou frauduleuse par l'Utilisateur des Services Professionnels ou des tiers nécessitant l'arrêt des Services Professionnels pour des raisons de sécurité ;
* à une utilisation des Services Professionnels non expressément autorisée par les CGV Services Professionnels ;
* à une intrusion ou à un maintien frauduleux d'un tiers dans le système, ou à l'extraction illicite de données, malgré la mise en œuvre des moyens de sécurisation conformes aux données actuelles de la technique, Misakey ne supportant qu'une obligation de moyens au regard des techniques connues de sécurisation ;
* à la nature et au contenu des informations, logiciels et données créées et/ou communiquées par l'Utilisateur des Services Professionnels ; plus généralement, Misakey ne peut en aucun cas être responsable à raison des données, informations, logiciels, résultats ou analyses provenant d'un tiers, transmises ou reçues au travers de l'utilisation des Services Professionnels ;
* à un retard dans l'acheminement des informations et données lorsque Misakey n'est pas à l'origine de ce retard ;
* au fonctionnement du réseau Internet ou des réseaux téléphoniques ou câblés d'accès à Internet non mis en œuvre par Misakey.

Misakey ne saurait être tenue responsable des dommages indirects ou ne résultant pas directement d'une défaillance des Services Professionnels.

Misakey ne sera tenue que de la réparation des dommages directs et prévisibles du fait de l'exécution des Services Professionnels.

En conséquence, Misakey ne pourra en aucune circonstance encourir de responsabilité au titre des dommages indirects ou imprévisibles de l'Utilisateur des Services Professionnels ou des tiers, ce qui inclut notamment tout gain manqué, perte, inexactitude ou corruption de fichiers ou de données, préjudice commercial, perte de chiffre d'affaires ou de bénéfice, perte de clientèle, perte d'une chance, coût de l'obtention d'un produit, d'un service ou de technologies de substitution, en relation ou provenant de l'inexécution ou de l'exécution fautive des Services Professionnels.

Le montant des dommages réparables que Misakey peut être amenée à payer à l'Utilisateur des Services Professionnels est limité, tous dommages confondus, au prix des Services Professionnels concernés à l'origine du dommage payé pour la période de 12 mois précédant le ou les événements ayant engendré une telle mise en cause de sa responsabilité au titre de ladite année.

### 9. Résiliation

L'Utilisateur des Services Professionnels peut à tout moment résilier sa relation contractuelle avec Misakey en utilisant la fonction « Supprimer le Compte » présente sur le Site Internet.

### 10. Propriété intellectuelle

L'Utilisateur des Services Professionnels concède à Misakey une licence non-exclusive, gratuite, mondiale et pour la durée des droits d'auteur en France aux fins de représenter, reproduire et mettre à disposition du public via ses Services les parties des Fiches créées par lui ainsi que toutes modifications qu'il y apporte.

Dans ce cadre, l'Utilisateur des Services Professionnels déclare qu'il jouit de la pleine capacité de consentir la présente licence ainsi que de tous les droits de propriété intellectuelle y afférents et que rien ne s'oppose de son chef à la conclusion de celle-ci. Il garantit Misakey contre tous troubles de jouissance provenant de son fait personnel ou du fait de tout tiers.

Par exception aux stipulations de l'article 4 des CGU Site Internet et Service de Consultation, Misakey concède à l'Utilisateur des Services Professionnels une licence non-exclusive, gratuite, mondiale et pour la durée des présentes aux fins de:
* télécharger les informations des Fiches publiques sous licence Creative Commons ([https://creativecommons.org/licenses/by/4.0/](https://creativecommons.org/licenses/by/4.0/)) (à l'exclusion des adresses email, commentaires et notes contenus dans les Fiches); et,
* modifier, éditer et compléter sa Fiche dans le cadre des Services Professionnels.

### 11. Droit applicable et juridiction compétente

Les CGV Services Professionnels sont régies par le droit français.

Pour tout litige relatif aux CGV Services Professionnels, les parties s'efforceront de trouver une solution amiable préalablement à toute procédure.

A défaut de règlement amiable, au terme d'un délai de dix (10) jours à compter de la transmission du litige, la Partie la plus diligente pourra saisir le Tribunal de Commerce de Paris, auquel il est expressément fait attribution de compétence, quel que soit le domicile du défendeur, ainsi qu'en cas d'appel en garantie ou de pluralité de défendeurs.

## DISPOSITIONS COMMUNES

### 1. Documents contractuels

La Documentation Contractuelle ainsi que la Politique de Confidentialité forment un tout indivisible et prévalent sur tout autre document. Elles constituent de ce fait l'intégralité des termes et conditions sur lesquels l'Utilisateur et Misakey se sont accordés. Elles remplacent tous documents, accords préalables et communications antérieures, y compris verbales ou de toute autre nature.

### 2. Durée

La présente Documentation Contractuelle s'applique pour une durée indéterminée.

Elle entre en vigueur, relativement aux documents qui la composent, à compter de la date de son acceptation par l'Utilisateur conformément aux CGU Site Internet et Service de Consultation, aux CGU Services de Gestion des Comptes Internet et Service Collaboratif de Création et de Modification des Fiches et aux CGV Services Professionnels.

### 3. Acceptation

La Documentation Contractuelle est communiquée à l'Utilisateur lors de sa connexion au Site Internet et est en outre disponibles lors de la création du Compte de Données Utilisateur.

Misakey se réserve le droit de modifier la Documentation Contractuelle de façon partielle ou totale, pour les besoins et selon l'évolution des Services ou du Site Internet, et ce, à tout moment. Toutefois, chaque document composant cette nouvelle Documentation Contractuelle ne s'appliquera que postérieurement à sa date d'entrée en vigueur, sauf si elle a été notifiée par Misakey et acceptée par l'Utilisateur.

L'Utilisateur certifie avoir la capacité d'accepter le document approprié de la Documentation Contractuelle et s'engage à le respecter.

S'il l'accepte au nom d'une personne morale, l'Utilisateur certifie qu'il en a le droit et l'autorité.

### 4. Traitement des données personnelles

Misakey s'engage à traiter les données à caractère personnel de l'Utilisateur, conformément à sa Politique de Confidentialité, et dans le respect des dispositions en vigueur de la législation sur la protection des données personnelles, et notamment de tout mettre en œuvre pour assurer la sécurité desdites données.

### 5. Obligations de Misakey

Misakey s'engage à mettre en œuvre ses meilleurs efforts pour permettre l'accès aux Services 24 heures sur 24 et 7 jours sur 7, sans pour autant qu'il en résulte une obligation de résultat à sa charge. L'Utilisateur reconnaît ainsi que Misakey est tenue à une obligation de moyens pour l'exécution des Services.

Les Services pourront néanmoins être interrompus sans information préalable des Utilisateurs en cas de force majeure, telle qu'une panne des Services.

### 6. Obligations de l'Utilisateur

Afin de bénéficier des Services, l'Utilisateur s'engage à :
* utiliser les Services conformément à leur destination et à la Documentation Contractuelle ;
* fournir toutes les informations requises concernant l'Utilisateur titulaire d'un Compte de Données Utilisateur ;
* exercer son activité conformément à la législation en vigueur ;
* respecter les droits des tiers et plus généralement la législation applicable aux Services ;
* ne pas engager d'action visant à interrompre, détruire, limiter ou nuire aux Services ou empêchant les autres Utilisateurs d'y accéder, notamment en utilisant des virus, des codes, des programmes ou des fichiers malveillants ;
* ne pas utiliser ou exploiter de Compte de Données Utilisateur dans un but autre que celui prévu à la Documentation Contractuelle.

### 7. Force majeure

La responsabilité de Misakey ne pourra être engagée ni recherchée dans l'hypothèse où l'exécution de l'une de ses obligations est empêchée ou retardée en raison d'un cas de force majeure, telle qu'apprécié par la jurisprudence et définie à l'article 1218 du Code civil.

### 8. Résiliation pour faute

En cas d'infraction à la législation en vigueur, de manquement à l'une quelconque des obligations énoncées dans la Documentation Contractuelle, ou plus généralement en cas de non-respect de celle-ci, Misakey pourra résilier de plein droit, après mise en demeure préalable de cesser le manquement restée sans effet pendant une durée de sept jours, les présentes.

Le cas échéant, Misakey pourra supprimer le Compte de Données Utilisateur de l'Utilisateur concerné.

### 9. Divisibilité

Dans l'hypothèse où une ou plusieurs dispositions de la Documentation Contractuelle, pour quelque motif que ce soit, déclarées nulles en tout ou en partie par une décision de justice revêtant l'autorité de la chose jugée, les autres dispositions resteraient en vigueur.

### 10. Renonciation

Le fait que Misakey ne fasse pas valoir un droit ou une disposition de la Documentation Contractuelle ne saurait être interprété comme une renonciation à s'en prévaloir ultérieurement.
